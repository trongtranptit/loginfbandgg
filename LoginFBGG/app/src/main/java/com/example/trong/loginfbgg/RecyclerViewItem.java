package com.example.trong.loginfbgg;

import java.io.Serializable;

/**
 * Created by trong on 7/20/2017.
 */

public class RecyclerViewItem implements Serializable {
    private String tittle;
    private int drawableURL;

    public RecyclerViewItem(String tittle, int drawableURL) {
        this.tittle = tittle;
        this.drawableURL = drawableURL;
    }

    public RecyclerViewItem() {
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public int getDrawableURL() {
        return drawableURL;
    }

    public void setDrawableURL(int drawableURL) {
        this.drawableURL = drawableURL;
    }
}
