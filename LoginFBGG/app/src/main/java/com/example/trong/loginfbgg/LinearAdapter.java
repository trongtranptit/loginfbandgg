package com.example.trong.loginfbgg;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by trong on 7/21/2017.
 */

public class LinearAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public final int TYPE_ITEM = 0;
    public static final int TYPE_LOAD = 1;
    public int layoutType = 0;
    RecyclerView recyclerView;
    private boolean isLoading;private int visibleThreshold = 5;private int lastVisibleItem, totalItemCount;
    Context context;
    ArrayList<RecyclerViewItem> data;
    OnLoadMoreListener mOnLoadMoreListener;
    public LinearAdapter(RecyclerView recyclerView, Context context, ArrayList<RecyclerViewItem> data) {
        this.recyclerView = recyclerView;
        this.context = context;
        this.data = data;

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if(layoutType==0){
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                }
                else if(layoutType==1){
                    LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                }
                else if(layoutType==2){
                    GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
                    totalItemCount = gridLayoutManager.getItemCount();
                    lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                }

                if (lastVisibleItem == (totalItemCount -1) && !isLoading){
                    if (mOnLoadMoreListener != null){
                        mOnLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setLayoutType(int layoutType){
        this.layoutType = layoutType;
    }
    @Override
    public int getItemViewType(int position) {
        return data.get(position)==null ? TYPE_LOAD : TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_ITEM){
            View v = LayoutInflater.from(context).inflate(R.layout.linear_item,parent,false);
            return new LinearHolder(v) ;
        }
        if(viewType == TYPE_LOAD){
            View v = LayoutInflater.from(context).inflate(R.layout.layout_loading_item,parent,false);
            return new LinearLoadingHolder(v) ;
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof LinearHolder){
            LinearHolder my_holder = (LinearHolder) holder;
            my_holder.icon.setImageResource(R.drawable.icon);
            my_holder.tv_title.setText(data.get(position).getTittle());
        }
        if(holder instanceof  LinearLoadingHolder){
            LinearLoadingHolder my_holder = (LinearLoadingHolder) holder;
            my_holder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener;
    }

    public void setLoaded(){
        isLoading= false;
    }

    public class LinearHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView tv_title;
        public LinearHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.imgIcon);
            tv_title = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }

    public class LinearLoadingHolder extends RecyclerView.ViewHolder{
        ProgressBar progressBar;
        public LinearLoadingHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.pb_loadmore);
        }
    }
}
