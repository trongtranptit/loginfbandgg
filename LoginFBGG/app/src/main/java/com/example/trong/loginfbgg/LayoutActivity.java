package com.example.trong.loginfbgg;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by trong on 7/20/2017.
 */

public class LayoutActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    LayoutInflater inflater;
    ArrayList<RecyclerViewItem> data = new ArrayList<RecyclerViewItem>();
    SwipeRefreshLayout swipeRefreshLayout;
    LinearAdapter adapter;
    boolean isLoading;
    public int layoutType = 0;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_layout);
        addData();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeResfreshLayout);
        getSupportActionBar().setTitle("Vertical Linear Layout");
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new LinearAdapter(recyclerView,this,data);
        recyclerView.setAdapter(adapter);
        setLoadMore();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);
                adapter.notifyDataSetChanged();
                for(int i=15;i<data.size();i++) data.remove(data.size()-1);
                if(layoutType==0) recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext(),LinearLayoutManager.VERTICAL,false));
                if(layoutType==1) recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext(),LinearLayoutManager.HORIZONTAL,false));
                if(layoutType==2) recyclerView.setLayoutManager(new GridLayoutManager(getBaseContext(),3));
                adapter.notifyDataSetChanged();
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }
    public void setLoadMore(){
        adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                data.add(null);
                isLoading = true;
                if(layoutType==2){
                    final GridLayoutManager manager = (GridLayoutManager) recyclerView.getLayoutManager();
                    manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            if(adapter.getItemViewType(position) == LinearAdapter.TYPE_LOAD) return manager.getSpanCount();
                            return 1;
                        }
                    });
                }
                adapter.notifyDataSetChanged();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        data.remove(data.size()-1);
                        adapter.notifyDataSetChanged();

                        int index = data.size();
                        if(index == 40){
                            recyclerView.removeAllViews();
                        }
                        else{
                            int end = 5+index; if(end > 40){
                                end = 40;

                            }
                            for(int i=index;i<=end;i++){
                                data.add(new RecyclerViewItem("tittle"+i,R.drawable.icon));
                            }
                            adapter.notifyDataSetChanged();
                            adapter.setLoaded();
                            isLoading = false;
                        }
                    }
                },2000);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    public void changeLayout(int layoutType){
        if(layoutType==0){
            getSupportActionBar().setTitle("Vertical Linear Layout");
            recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
            this.layoutType = 0;
            adapter.setLayoutType(0);
        }
        else if(layoutType == 1){
            getSupportActionBar().setTitle("Horizontal Linear Layout");
            recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL,false));
            this.layoutType = 1;
            adapter.setLayoutType(1);
        }
        else if(layoutType ==2){
            getSupportActionBar().setTitle("Grid Layout");
            final GridLayoutManager manager = new GridLayoutManager(this,3);
            manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                @Override
                public int getSpanSize(int position) {
                    if(adapter.getItemViewType(position) == LinearAdapter.TYPE_LOAD) return manager.getSpanCount();
                    return 1;
                }
            });
            recyclerView.setLayoutManager(manager);
            this.layoutType = 2;
            adapter.setLayoutType(2);
        }
    }
    private void addData() {
        data = new ArrayList<>();
        for(int i=0;i<15;i++){
            RecyclerViewItem model = new RecyclerViewItem("tittle "+i,R.drawable.icon);
            data.add(model);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.linear_vertical){
            //addData();
            changeLayout(0);
        }
        if(id==R.id.linear_horizontal){
            //addData();
            changeLayout(1);
        }
        if(id==R.id.grid){
            //addData();
            changeLayout(2);
        }
        return super.onOptionsItemSelected(item);
    }




}
